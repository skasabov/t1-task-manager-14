package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}

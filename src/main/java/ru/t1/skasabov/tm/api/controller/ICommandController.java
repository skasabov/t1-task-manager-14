package ru.t1.skasabov.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showSystemInfo();

    void showErrorArgument(String argument);

    void showErrorCommand(String command);

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}

package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name, String description);

    Project create(String name);

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Boolean existsById(String id);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    void removeById(String id);

    int getSize();

}

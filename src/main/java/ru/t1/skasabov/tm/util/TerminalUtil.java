package ru.t1.skasabov.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return 0;
        for (int i = 0; i < value.length(); i++) {
            final char symbol = value.charAt(i);
            if (!Character.isDigit(symbol)) return 0;
        }
        return Integer.parseInt(value);
    }

}
